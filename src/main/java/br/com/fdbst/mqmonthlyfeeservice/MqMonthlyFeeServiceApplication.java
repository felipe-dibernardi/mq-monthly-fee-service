package br.com.fdbst.mqmonthlyfeeservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MqMonthlyFeeServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MqMonthlyFeeServiceApplication.class, args);
    }

}
