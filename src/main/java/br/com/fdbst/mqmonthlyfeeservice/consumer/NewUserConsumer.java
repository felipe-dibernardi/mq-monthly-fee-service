package br.com.fdbst.mqmonthlyfeeservice.consumer;

import org.springframework.stereotype.Component;

@Component
public class NewUserConsumer {

    public void receiveNewUser(String newUser) {
        System.out.println("Message received: " + newUser);
    }

}
