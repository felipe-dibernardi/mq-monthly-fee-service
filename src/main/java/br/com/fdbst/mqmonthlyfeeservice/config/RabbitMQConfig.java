package br.com.fdbst.mqmonthlyfeeservice.config;

import br.com.fdbst.mqmonthlyfeeservice.consumer.NewUserConsumer;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    @Value("topic.exchange.new-user")
    private String topicExchange;

    @Value("queue.monthly-fee")
    private String createMonthlyFeeQueue;

    @Value("routing.key.new-user.monthly-fee")
    private String newUserMonthlyFeeRoutingKey;

    @Bean
    Queue consumerQueue() {
        return new Queue(createMonthlyFeeQueue, false);
    }

    @Bean
    TopicExchange topicExchange() {
        return new TopicExchange(topicExchange);
    }

    @Bean
    Binding binding(Queue queue, TopicExchange topicExchange) {
        return BindingBuilder.bind(queue).to(topicExchange).with(newUserMonthlyFeeRoutingKey);
    }

    @Bean
    MessageListenerAdapter messageListenerAdapter(NewUserConsumer consumer) {
        return new MessageListenerAdapter(consumer, "receiveNewUser");
    }

    @Bean
    SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
                                             MessageListenerAdapter messageListenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(createMonthlyFeeQueue);
        container.setMessageListener(messageListenerAdapter);
        return container;
    }

    @Bean
    public AmqpAdmin amqpAdmin(ConnectionFactory connectionFactory) {
        return new RabbitAdmin(connectionFactory);
    }

}
